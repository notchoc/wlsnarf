.POSIX:
PREFIX     = /usr/local
PKG_CONFIG = pkg-config
LDLIBS     = `$(PKG_CONFIG) --libs wayland-client`

all: wlsnarf

.c.o:
	$(CC) -Wall -Wextra -Wno-unused-parameter $(CPPFLAGS) $(WLSCFLAGS) -c $<

wlsnarf: wlsnarf.o wlr-data-control-unstable-v1-protocol.o
	$(CC) wlsnarf.o wlr-data-control-unstable-v1-protocol.o $(LDLIBS) $(LDFLAGS) -o $@
wlsnarf.o: wlsnarf.c wlr-data-control-unstable-v1-protocol.h
wlr-data-control-unstable-v1-protocol.o: wlr-data-control-unstable-v1-protocol.h

WAYLAND_SCANNER   = `$(PKG_CONFIG) --variable=wayland_scanner wayland-scanner`
WAYLAND_PROTOCOLS = `$(PKG_CONFIG) --variable=pkgdatadir wayland-protocols`

wlr-data-control-unstable-v1-protocol.h:
	$(WAYLAND_SCANNER) client-header \
		protocols/wlr-data-control-unstable-v1.xml $@
wlr-data-control-unstable-v1-protocol.c:
	$(WAYLAND_SCANNER) private-code \
		protocols/wlr-data-control-unstable-v1.xml $@

clean:
	rm -f wlsnarf *.o *-protocol.*

install: wlsnarf
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f wlsnarf $(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/wlsnarf

install-shim: wlsnarf
	cp -f wl-clipboard-shim/wl-copy wl-clipboard-shim/wl-paste \
		$(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/wl-copy \
		$(DESTDIR)$(PREFIX)/bin/wl-paste

install-snarfsrv: wlsnarf
	cp -f snarfsrv/snarfsrv snarfsrv/snarftofile \
		$(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/snarfsrv \
		$(DESTDIR)$(PREFIX)/bin/snarftofile

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/wlsnarf

uninstall-shim:
	rm -f $(DESTDIR)$(PREFIX)/bin/wl-copy \
		$(DESTDIR)$(PREFIX)/bin/wl-paste

uninstall-snarfsrv:
	rm -f $(DESTDIR)$(PREFIX)/bin/snarfsrv \
		$(DESTDIR)$(PREFIX)/bin/snarftofile

.PHONY: all clean install uninstall
