#include <err.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <wayland-client.h>

#include "arg.h"
#include "wlr-data-control-unstable-v1-protocol.h"

char *argv0;

static int run = 1;
static bool pflag;
static bool oflag;
static bool cflag;
static char *seatname;
static char **watchcmd;
static size_t watchcmd_len;

static struct wl_display *display;
static struct wl_registry *registry;
static struct wl_seat *seat;
static struct zwlr_data_control_manager_v1 *data_control_manager;
static struct zwlr_data_control_device_v1 *data_device;
static struct zwlr_data_control_source_v1 *data_source;

#define MIN(a, b)	((a) < (b) ? (a) : (b))
#define die(fmt, ...)	errx(EXIT_FAILURE, fmt, ##__VA_ARGS__)

static void noop() {}

static void
seat_name(void *data, struct wl_seat *new_seat, const char *name)
{
	if (!seat && strcmp(name, seatname) == 0)
		seat = new_seat;
	else
		wl_seat_destroy(new_seat);
}

static const struct wl_seat_listener seat_listener = {
	.name = seat_name,
	.capabilities = noop,
};

static void
registry_global(void *data, struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version)
{
	if (!seat && strcmp(interface, "wl_seat") == 0) {
		seat = wl_registry_bind(registry, name, &wl_seat_interface, MIN(WL_SEAT_NAME_SINCE_VERSION, version));
		if (seatname) {
			wl_seat_add_listener(seat, &seat_listener, NULL);
			seat = NULL;
		}
	} else if (strcmp(interface, "zwlr_data_control_manager_v1") == 0) {
		data_control_manager = wl_registry_bind(registry, name, &zwlr_data_control_manager_v1_interface,
			MIN(ZWLR_DATA_CONTROL_DEVICE_V1_PRIMARY_SELECTION_SINCE_VERSION, version));
	}
}

static const struct wl_registry_listener registry_listener = {
	.global = registry_global,
	.global_remove = noop,
};

static void
data_source_send(void *data, struct zwlr_data_control_source_v1 *source, const char *mime_type, int32_t fd)
{
	bool mimed = false;
	char *path = NULL;

	for (char **v = (char **)data; *v; v++) {
		for (char *c = &v[0][1]; *c; c++) {
			char *argf = c[1]?c+1:*++v;
			switch (*c) {
			case 't':
				if (!mimed && strcmp(argf, mime_type) == 0)
					mimed = true;
				continue;
			case 'i':
				if (mimed == (mime_type[0] == '\0'))
					continue;
				path = argf;
				goto found;
			}
		}
	}
	if (!mimed) {
		warn("bad mimetype %s", mime_type);
		close(fd);
		return;
	}

found:

	if (!path || strcmp(path, "-") == 0)
		path = "/dev/stdin";

	pid_t pid = fork();

	if (pid < 0) warn("fork");

	if (pid == 0) {
		dup2(fd, STDOUT_FILENO);
		close(fd);
		signal(SIGHUP, SIG_DFL);
		signal(SIGPIPE, SIG_DFL);
		execl("/bin/cat", "/bin/cat", "--", path, NULL);
		warn("exec /bin/cat");
	}

	close(fd);

	if (oflag) {
		wait(NULL);
		run = 0;
	}
}

static void
data_source_cancelled(void *data, struct zwlr_data_control_source_v1 *source)
{
	run = 0;
}

static const struct zwlr_data_control_source_v1_listener data_source_listener = {
	.send = data_source_send,
	.cancelled = data_source_cancelled,
};

static void
receive(struct wl_array *arr, bool cond, struct zwlr_data_control_offer_v1 *offer)
{
	if (cond) {
		if (!offer) {
			if (oflag) run = -1;
			return;
		}

		char *s;
		wl_array_for_each(s, arr) {
			int pipes[2];
			if (pipe(pipes) == -1) {
				warn("bad pipe");
				continue;
			}

			zwlr_data_control_offer_v1_receive(offer, s, pipes[1]);
			wl_display_flush(display);

			pid_t pid = fork();

			if (pid < 0) warn("fork");

			if (pid == 0) {
				close(pipes[1]);
				dup2(pipes[0], STDIN_FILENO);
				close(pipes[0]);

				signal(SIGHUP, SIG_DFL);
				signal(SIGPIPE, SIG_DFL);
				watchcmd[watchcmd_len] = s;
				execvp(watchcmd[0], watchcmd);
				warn("exec %s", watchcmd[0]);
			}

			close(pipes[1]);
			close(pipes[0]);

			while (*s) s++;
		}

		if (oflag) {
			while (wait(NULL) != -1);
			run = 0;
		}
	}

	if (offer)
		zwlr_data_control_offer_v1_destroy(offer);

	arr->size = 0;
}

static void
offer_offer(void *data, struct zwlr_data_control_offer_v1 *offer, const char *mime_type)
{
	struct wl_array *arr = data;
	size_t len = strlen(mime_type) + 1;
	strcpy(wl_array_add(arr, len), mime_type);
}

static const struct zwlr_data_control_offer_v1_listener offer_listener = {
	.offer = offer_offer,
};

static void
control_data_offer(void *data, struct zwlr_data_control_device_v1 *device, struct zwlr_data_control_offer_v1 *offer)
{
	zwlr_data_control_offer_v1_add_listener(offer, &offer_listener, data);
}

static void
control_data_selection(void *data, struct zwlr_data_control_device_v1 *device, struct zwlr_data_control_offer_v1 *offer)
{
	receive((struct wl_array *)data, !pflag, offer);
}

static void
control_data_primary_selection(void *data, struct zwlr_data_control_device_v1 *device, struct zwlr_data_control_offer_v1 *offer)
{
	receive((struct wl_array *)data, pflag, offer);
}

static const struct zwlr_data_control_device_v1_listener device_listener = {
	.data_offer = control_data_offer,
	.selection = control_data_selection,
	.primary_selection = control_data_primary_selection,
};

_Noreturn static void
usage(int code)
{
	FILE *out = code ? stderr : stdout;
	fprintf(out, "usage:");
	fprintf(out, "\t%s [-s <seat>] [-p] [-o] (-i <file> [-t <mimetype>]...)...\n", argv0);
	fprintf(out, "\t%s [-s <seat>] [-p] [-o] -w <cmd> <args>...\n", argv0);
	fprintf(out, "\t%s [-s <seat>] [-p] -c\n", argv0);
	exit(code);
}

int
main(int argc, char *argv[])
{
	ARGBEGIN {
	case '?': case 'h':
		usage(0);
	case 's': seatname = EARGF(usage(1)); break;
	case 'p': pflag = true; break;
	case 'o': oflag = true; break;
	case 'c': cflag = true; break;
	case 't': case 'i':
		*--argv[0] = '-';
		goto argend;
	case 'w':
		if (!*++argv[0])
			argv++, argc--;
		watchcmd_len = argc;
		watchcmd = argv - 1;
		goto argend;
	default:
		warnx("bad option: -%c", ARGC());
		usage(1);
	} ARGEND;
	if (argv[0] == NULL && !cflag) usage(1);
argend:

	if (!(display = wl_display_connect(NULL)))
		die("bad display");

	if (!(registry = wl_display_get_registry(display)))
		die("bad registry");

	wl_registry_add_listener(registry, &registry_listener, NULL);

	wl_display_roundtrip(display);
	if (seatname)
		wl_display_roundtrip(display);

	if (!seat)
		die("bad seat");

	if (!data_control_manager)
		die("bad data control manager");
	
	if (!(data_device = zwlr_data_control_manager_v1_get_data_device(data_control_manager, seat)))
		die("bad data device");

	if (cflag) {
		run = 0;
		goto set_selection;
	}

	if (!oflag && signal(SIGCHLD, SIG_IGN) == SIG_ERR)
		die("bad ignore SIGCHLD");

	if (watchcmd) {
		// shift argv back to make space at the end
		for (argv = watchcmd; *argv; argv++)
			argv[0] = argv[1];
		struct wl_array mimetypes;
		wl_array_init(&mimetypes);
		zwlr_data_control_device_v1_add_listener(data_device, &device_listener, &mimetypes);
	} else {
		if (!(data_source = zwlr_data_control_manager_v1_create_data_source(data_control_manager)))
			die("bad data source");

		bool mimed = 0;
		for (char **v = argv; *v; v++) {
			if (!(v[0][0] == '-' && v[0][1]))
				usage(1);
			char *c = &v[0][1];
			char *argf = c[1]?c+1:*++v?*v:(usage(1),(char*)0);
			switch (*c) {
			case 't':
				mimed = true;
				zwlr_data_control_source_v1_offer(data_source, argf);
				break;
			case 'i':
				if (mimed)
					mimed = false;
				else
					zwlr_data_control_source_v1_offer(data_source, "");
				break;
			default:
				warnx("bad option: -%c", *c);
				usage(1);
			}
		}

		zwlr_data_control_source_v1_add_listener(data_source, &data_source_listener, argv);

set_selection:
		if (pflag)
			zwlr_data_control_device_v1_set_primary_selection(data_device, data_source);
		else
			zwlr_data_control_device_v1_set_selection(data_device, data_source);
	}

	while (wl_display_dispatch(display) != -1 && run > 0);

	return -run;
}
