#!/bin/sh
# wl-paste - paste content from wayland clipboard (wlsnarf shim)
# usage: wl-paste [OPTIONS]

fmt() { local f=; [ $# -gt 1 ] && f="$1" && shift || f='%s'; printf "$f\n" "$@"; }
die() { fmt "$@" >&2; exit 1; }

O="$(getopt -n "$0" -l 'help no-newline list-types primary watch type: seat:' -- +hnlpwt:s: "$@")" || exit 1
eval set -- "$O"
while [ $# -gt 0 ]; do
	case "$1" in # OPTIONS
	-n|--no-newline)	nonewline=1 ;;			# don't append newline
	-l|--list-types)	list=1 ;;			# list available mimetypes
	-p|--primary)		primary=1 ;;			# use primary clipboard
	-w|--watch)		command=1; shift; break ;;	# run command on selection change
	-t|--type)		type="$2"; shift ;;		# select mimetype (only uses last)
	-s|--seat)		seat="$2"; shift ;;		# select wayland seat
	-h|--help)		cat <<-. ; exit ;;		# print this help
				usage:	wl-paste [-nlpwh] [-t mime/type] [-s seat]
				.
	--)			shift; break ;;
	*)			die "$0: unrecognized option: $1" ;;
	esac
	shift
done

save() {
	for i do printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/" ; done
	[ $# -gt 0 ] && echo " "
}

watchcmd="$(save "$@")"

set -- wlsnarf ${primary:+-p} ${seat:+-s "$seat"}

if [ -n "$list" ]; then
	"$@" -o -w echo
	exit
fi

mimetest='[ x"$2" = x"$1" ]'
if [ -n "$command" ]; then
	command='eval exec "$0"'
	case "$type" in
	'')	mimetest='' ;;
	text|text/plain|TEXT|STRING|UTF8_STRING)
		mimetest='case "$1" in text|text/plain|TEXT|STRING|UTF8_STRING);; *) exit 1;; esac'
		;;
	*/*)	;;
	*)	mimetest='[ x"${2%%/*}" = x"$1" ]' ;;
	esac
else
	once=1
	case "${type:=text}" in
	text|text/plain|TEXT|STRING|UTF8_STRING)
		types="$("$@" -o -w echo)"
		for type in "$type" text/plain text TEXT STRING UTF8_STRING ''; do
			[ -z "$type" ] && return 1
			case $'\n'"$types"$'\n' in
			*$'\n'"$type"$'\n'*) break 2 ;;
			esac
		done
		;;
	*/*)	;;
	*)	type="$(wlsnarf -o -w echo | grep -m1 "^$type/")" || exit
	esac
	command='cat'
	[ -z "$nonewline" ] && command="$command && printf '\n'"
fi

"$@" ${once:+-o} -w sh -c "${mimetest:+$mimetest && }$command" "$watchcmd" "$type"
